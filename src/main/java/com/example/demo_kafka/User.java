package com.example.demo_kafka;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class User {

    @Id
    private Integer id;

    @Column
    private String name;

}
