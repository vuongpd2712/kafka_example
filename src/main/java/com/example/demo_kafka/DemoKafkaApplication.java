package com.example.demo_kafka;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;

@SpringBootApplication
@Slf4j
public class DemoKafkaApplication implements CommandLineRunner {
    @Autowired
    UserRepo userx;

    @Autowired
    KafkaTemplate<String, String> kafkaTemplate;

    @KafkaListener(topics = "demo", groupId = "group-id")
    public void listen(String message) {

        Gson gson = new Gson();
        User user = gson.fromJson(message, User.class);
        userx.save(user);
        log.info("done");
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoKafkaApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Gson gson = new Gson();
        User u = new User();
        u.setId(1);
        u.setName("Vuong");
        String mes = gson.toJson(u);
        kafkaTemplate.send("demo", mes);
    }
}
